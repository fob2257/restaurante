var orden = [];//0-id 1-nombre 2-precio 3-precioiva 4-cantidad
var numProductos=0;
var carrito=document.getElementsByName("carrito")[0];
var importe=0;

function introducePlatillo(id,nombre,precio){
	id = parseInt(id);
	precio = parseInt(precio);
	var cantidad = parseInt(document.getElementsByName(id)[0].value);
	var a = existePlatillo(id);
	if(a == -1){
		var tabla = document.getElementsByName("carrito")[0];
		var row = tabla.insertRow();
		precioiva = parseInt(Math.ceil(precio*1.15));
		orden.push([id,nombre,precio,precioiva,cantidad]);
	    var cell0 = row.insertCell(0);
	    var cell1 = row.insertCell(1);
	    var cell2 = row.insertCell(2);
	    var cell3 = row.insertCell(3);
	    var cell4 = row.insertCell(4);
	    var cell5 = row.insertCell(5);

	    cell0.innerHTML = orden[orden.length-1][1];
	    cell1.innerHTML = "$"+orden[orden.length-1][2];
	    cell2.innerHTML = "$"+orden[orden.length-1][3];
	    cell3.innerHTML = orden[orden.length-1][4];
	    cell4.innerHTML = "$"+((orden[orden.length-1][4])*(orden[orden.length-1][3]));
	    cell5.innerHTML = '<button type="button" class="btn btn-danger" onclick="quitaPlatillo('+orden[orden.length-1][0]+')">\
                          	<span class="glyphicon glyphicon-remove"></span> Quitar\
                      		</button></td>';

	}else{
		orden[a][4] += cantidad;
		var fila = document.getElementsByName("carrito")[0].rows[a].cells[3].innerHTML=orden[a][4];
		var fila1 = document.getElementsByName("carrito")[0].rows[a].cells[4].innerHTML="$"+(orden[a][4]*orden[a][3]);
	}
	calculaImporte();
}

function existePlatillo(id){
	for (var i = 0; i < orden.length; i++) {
		if(orden[i][0] == id){
			return i;
		}
	};
	return -1;
}

function quitaPlatillo(id){
	for (var i = 0; i < orden.length; i++) {
		if(orden[i][0] == id){
			if (orden[i][4] == 1) {
				orden.splice(i,1);
				var tabla = document.getElementsByName("carrito")[0].deleteRow(i);
			}else{
				orden[i][4]--;
				var fila = document.getElementsByName("carrito")[0].rows[i].cells[3].innerHTML=orden[i][4];
				var fila2 = document.getElementsByName("carrito")[0].rows[i].cells[4].innerHTML="$"+orden[i][4]*orden[i][3];
			}
		}
	};
	calculaImporte();
}

function calculaImporte(){
	importe=0;
	for (var i = 0; i < orden.length; i++) {
		importe += orden[i][3]*orden[i][4];
	};
	var fila = document.getElementsByName("total")[0].rows[0].cells[4].innerHTML="$"+importe;
}

function enviarOrden() {
	var nombre=document.getElementsByName("nombre")[0].value;
	var direccion=document.getElementsByName("direccion")[0].value;
	var telefono=document.getElementsByName("telefono")[0].value;
	calculaImporte();
	$.post('sendInfo', {
		_token: $('meta[name=csrf-token]').attr('content'),
		arrayOrden: JSON.stringify(orden),
		importe: importe,
		estado: 0,
		nombre: nombre,
		direccion: direccion,
		telefono: telefono
	})
	.done(function(data) {
      return data;
    })
    .fail(function() {
      return "error";
    });
}