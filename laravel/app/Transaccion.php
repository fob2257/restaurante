<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
	protected $table = 'transaccion';
    protected $primaryKey = 'idtransaccion';
    public static $key = 'idtransaccion';
    public $timestamps = false;
    protected $fillable = [
    'idtransaccion',
    'exitosa',
    'numerotransaccion',
    'importe',
    'venta_idventa'];
}
