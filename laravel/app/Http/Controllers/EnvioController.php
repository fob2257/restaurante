<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Transaccion;
use App\Ticket;
use View;
use DB;
use Auth;
use App\Orden;
use App\Pedido;
use App\Venta;
use App\Producto;
use App\Factura;
//use App\Orden;

class EnvioController extends Controller
{
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'numtarjeta' => 'required|min:16|max:16',
      'month' => 'required|min:1|max:2',
      'year' => 'required|min:2|max:2',
      'verificacion' => 'required|min:3|max:3'
    ]);

    if ($validator->fails()) {
      return redirect('domicilio')->withErrors($validator)->withInput();
    }

    $month = $request->input('month');

    if ((int)$month < 10) {
      $month = "0".$month;
    }

    $url = 'http://drumarcco-001-site1.itempurl.com/api/Transaction';

    $cardN=$request->input('numtarjeta');
    $expDate=$month."/".$request->input('year');
    $secCode=$request->input('verificacion');
    $token="9c6643f8-9393-4b2e-a87d-d876d4ceda77";
    $amount=$request->input('importe');
    $data = array('CardNumber' => $cardN, 'ExpirationDate' => $expDate, 'SecurityCode' => $secCode, 'Token' => $token, 'Amount' => $amount);
    $data_string = json_encode($data);
    
    $ch = curl_init( $url );
    curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                'Content-Length:' . strlen($data_string))
    );
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,1); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 400); //timeout in seconds

    $response = curl_exec( $ch );
    $resCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    $idLastVenta = \App\Venta::orderBy('idventa', 'desc')->first()->idventa;

    if ($resCode == 200) {
      $newTrans = new Transaccion;
      $newTrans->exitosa = 2;
      $newTrans->numerotransaccion = $response;
      $newTrans->importe = $request->input('importe');
      $newTrans->venta_idventa = $request->input('venta');
      $newTrans->save();

      $idLastTransaccion = \App\Transaccion::orderBy('idtransaccion', 'desc')->first()->idtransaccion;

      $newTicket = new Ticket;
      $newTicket->transaccion_idtransaccion = $idLastTransaccion;
      $newTicket->save();

      DB::table('venta')
      ->where('idventa', $idLastVenta)
      ->update(array('estado' => 2));

      return redirect ('ticket')->with('status', 'Pago realizado');
    }else{
      $newTrans = new Transaccion;
      $newTrans->exitosa = 1;
      $newTrans->numerotransaccion = "NA";
      $newTrans->importe = $request->input('importe');
      $newTrans->venta_idventa = $request->input('venta');
      $newTrans->save();

      $idLastTransaccion = \App\Transaccion::orderBy('idtransaccion', 'desc')->first()->idtransaccion;

      $newTicket = new Ticket;
      $newTicket->transaccion_idtransaccion = $idLastTransaccion;
      $newTicket->save();

      DB::table('venta')                //Guarda respuestas en la BD
      ->where('idventa', $idLastVenta)
      ->update(array('estado' => 1));
    }

    return redirect ('ticket')->with('status', 'Pago fallido');
    //return ('respuesta:'.$response."data_string".$data_string);
    //return ($data_string);
  }

  public function getTickets()
  {
    $userid = Auth::user()->idcliente;
      if (empty(DB::table('venta')->where('cliente_idcliente', $userid)->get())) {
        return View::make('ticket');
      }else{
        $venta = DB::table('venta')->where('cliente_idcliente', $userid)->get();
      }
    $venta = DB::table('venta')->where('cliente_idcliente', $userid)->get();
    
    foreach ($venta as $ven => $value) {
      $datosTicket[$ven][0] = $value->importe;
      $datosTicket[$ven][1] = $value->fecharegistro;
      $datosTicket[$ven][2] = $value->estado;
      $datosTicket[$ven][7] = $value->idventa;
      
      $productos = DB::table('producto')->where('venta_idventa', $value->idventa)->get();
      foreach ($productos as $key => $val) {
        $datosPro[$ven][$key][0] = $val->importe;
        $datosPro[$ven][$key][1] = $val->cantidad;
        $datosPro[$ven][$key][2] = $val->descripcion;
      }

      if (empty(DB::table('transaccion')->select('idtransaccion')->where('venta_idventa', $value->idventa)->first()->idtransaccion)) {
        $datosTicket[$ven][3] = 0;
        $datosTicket[$ven][4] = 0;
        $datosTicket[$ven][5] = 0;
        $datosTicket[$ven][6] = 0;
        continue;
      }

      $datosTicket[$ven][3] = DB::table('transaccion')->select('idtransaccion')->where('venta_idventa', $value->idventa)->first()->idtransaccion;
      $datosTicket[$ven][4] = DB::table('transaccion')->select('numerotransaccion')->where('venta_idventa', $value->idventa)->first()->numerotransaccion;
      $datosTicket[$ven][5] = DB::table('transaccion')->select('exitosa')->where('venta_idventa', $value->idventa)->first()->exitosa;
      if (empty(DB::table('ticket')->select('no_folio')->where('transaccion_idtransaccion', $datosTicket[$ven][3])->first()->no_folio)) {
        $datosTicket[$ven][6] = 0;
      }else{
        $datosTicket[$ven][6] = DB::table('ticket')->select('no_folio')->where('transaccion_idtransaccion', $datosTicket[$ven][3])->first()->no_folio;
      }
    }

    return View::make('ticket')->with('datosTicket', $datosTicket)->with('datosPro', $datosPro);

  }

  public function getFacturas($id)
  {
    $userid = Auth::user()->idcliente;
    $producto = DB::table('producto')->where('venta_idventa', $id)->get();
    $importe = DB::table('venta')->select('importe')->where('idventa', $id)->first()->importe;
    $newFactura = new Factura;
    $newFactura->rfccliente = "";
    $newFactura->ticket_no_folio = 1;
    $newFactura->datosfiscales_RFC = "RESFRA200516";
    $newFactura->save();

    $idLastFactura = \App\Factura::orderBy('no_folio', 'desc')->first()->no_folio;
    $fecha = \App\Factura::orderBy('fecharegistro', 'desc')->first()->fecharegistro;
    $domicilio= DB::table('envio')->select('direccion')->where('venta_idventa', $id)->first()->direccion;
    //return view('facturas')->with('id', $idVenta)->with('producto', $producto);
    return view('facturas')->with('id', $id)->with('producto', $producto)->with('domicilio', $domicilio)->with('folio', $idLastFactura)->with('importe', $importe)->with('fecha', $fecha);
  }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
