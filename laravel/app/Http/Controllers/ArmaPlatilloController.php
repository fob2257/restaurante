<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use View;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use App\Orden;
use App\Pedido;
use App\Venta;
use App\Producto;
use Orchestra\Parser\Reader as BaseReader;
use Orchestra\Parser\InvalidContentException;

use File;
use Response; //agregar
//use Input; //agregar
class ArmaPlatilloController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPlatillos()
    {
        $platillo = DB::table('platillo')->get();
        return View::make('armarPlatillo')->with('platillo', $platillo);

    }

    public function getDatos()
    {
        $userid = Auth::user()->idcliente;
        $lastImporte = Orden::orderBy('idorden', 'desc')->first()->importe;
        $idLastVenta = Venta::orderBy('idventa', 'desc')->first()->idventa;
        return View::make('domicilio')->with('id', $userid)->with('importe',$lastImporte)->with('venta',$idLastVenta);

    }

    public function insertaOrden(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'nombre' => 'required|min:3|max:30',
          'direccion' => 'required|min:3|max:50',
          'telefono' => 'required|min:10|max:10'
        ]);

        if ($validator->fails()) {
          return redirect('armar')->withErrors($validator)->withInput();
        }

        $idLastVenta = \App\Venta::orderBy('idventa', 'desc')->first()->idventa;
        \App\Envio::create(
          [
          'nombre' => $request['nombre'],
          'direccion'=> $request['direccion'],
          'venta_idventa'=> $idLastVenta,
          'telefono' => $request['telefono']
          ]
        );

        $userid = Auth::user()->idcliente;
        $orden = $request->input('arrayOrden');
        $estado = $request->input('estado');
        $importe = $request->input('importe');

        //Inserta orden(servicio registro y pedido)
        $newOrden = new Orden;
        $newOrden->importe = $importe;
        $newOrden->estado = $estado;
        $newOrden->cliente_idcliente = $userid;
        $newOrden->save();

        //Inserta Venta(servicio 2)
        $newVenta = new Venta;
        $newVenta->importe = $importe;
        $newVenta->estado = $estado;
        $newVenta->cliente_idcliente = $userid;
        $newVenta->save();

        //Inserta pedidos(servicio registro y pedido)
        $idLastOrder = Orden::orderBy('idorden', 'desc')->first()->idorden;
        $idLastVenta = Venta::orderBy('idventa', 'desc')->first()->idventa;
        $orden = json_decode($orden, true);
        $longitud = sizeof($orden);
        for ($i=0; $i < $longitud; $i++) {
            $newPedido = new Pedido;
            $newPedido->cantidad = $orden[$i][4];
            $newPedido->precio = $orden[$i][3];
            $newPedido->orden_idorden = $idLastOrder;
            $newPedido->orden_cliente_idcliente = $userid;
            $newPedido->extra_idextra = 1;
            $newPedido->platillo_idplatillo = $orden[$i][0];
            $newPedido->save();

            //Inserta productos(servicio 2)
            $newProducto = new Producto;
            $newProducto->cantidad = $orden[$i][4];
            $newProducto->importe = $orden[$i][3]*$orden[$i][4];
            $newProducto->descripcion = $orden[$i][1];
            $newProducto->venta_idventa = $idLastVenta;
            $newProducto->save();
        }
    }
}