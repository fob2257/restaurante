<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;


class ClienteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|min:3|max:45',
            'apellidos' => 'required|min:3|max:45',
            'telefono' => 'required|min:10|max:10',
            'email' => 'required|email|max:255|unique:cliente',
            'password' => 'required|confirmed|min:6|max:30',
            ]);

        if ($validator->fails()) {
            return redirect('registrarse')
            ->withErrors($validator)
            ->withInput();
        }
        
        \App\Cliente::create([
            'nombre' => $request['nombre'],
            'apellidos'=> $request['apellidos'],
            'telefono' => $request['telefono'],
            'email' => $request['email'],
            'password' => bcrypt($request['password'])
            ]);
        return redirect ('auth/login')->with('status', 'Registro finalizado correctamente! Por favor inicia sesión ');
    }
}
