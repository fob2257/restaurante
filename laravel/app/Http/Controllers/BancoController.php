<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Response; //agregar

class BancoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bancoTransferencia(Request $request)
    {
        $url = 'http://189.170.144.90:8080/api/Transaction';
        $cardN=$request->input('numCard');
        $expDate=$request->input('vigencia');
        $secCode=$request->input('verificacion');
        $token="9c6643f8-9393-4b2e-a87d-d876d4ceda77";
        $amount=115;

        $data = array('CardNumber' => $cardN, 'ExpirationDate' => $expDate, 'SecurityCode' => $secCode, 'Token' => $token, 'Amount' => $amount);
        $data_string = json_encode($data);
        $ch = curl_init( $url );
        //curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                    'Content-Length:' . strlen($data_string))
        );

        $response = curl_exec( $ch );

        return ('respuesta:'.$response."data_string".$data_string);
    }
}
