<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
  return view('home');
});
/*Route::get('/facturas', function () {
  return view('facturas');
});*/
/*Route::get('/ticket', function () {
    return view('ticket');
});*/
Route::get('/clientes', function () {
  return view('clientes');
});
Route::get('/armar', function () {
  return view('armarPlatillo');
});

//Route::get('/factura', 'EnvioController@getFacturas');
Route::get('/ticket', 'EnvioController@getTickets');
Route::get('/factura-{id}','EnvioController@getFacturas');
Route::get('/domicilio', 'ArmaPlatilloController@getDatos');

Route::get('/banco', function () {
  return view('banco');
});
Route::get('/armar','ArmaPlatilloController@getPlatillos');
Route::post('/sendInfo', 'ArmaPlatilloController@insertaOrden');

Route::resource('cliente','ClienteController');
Route::resource('envio','EnvioController');
Route::resource('bancoTransfer','BancoController@bancoTransferencia');

Route::get('/registrarse', function () {
  return view('auth/registrarse');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);
