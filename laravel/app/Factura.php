<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
	protected $table = 'factura';
  protected $primaryKey = 'no_folio';
  public static $key = 'no_folio';
  public $timestamps = false;
  protected $fillable = [
  'rfccliente',
  'ticket_no_folio',
  'datosfiscales_RFC'];

    public function ticket()
	{
		return $this->belongsTo('App\Ticket');
	}
}
