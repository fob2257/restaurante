<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{

    //
    protected $table = 'producto';
    public $timestamps = false;
    protected $fillable = [
    'idproducto',
    'cantidad',
    'importe',
    'descripcion',
    'venta_idventa'];

	public function orden()
	{	
		return $this->belongsTo('App\Venta', 'venta_idventa	');
	}
}
