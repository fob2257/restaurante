<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    //
    protected $table = 'ingrediente';

         public function platillos()
    {
        return $this->belongsToMany('App\Platillo');
    }
}
}
