<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cliente extends Authenticatable
{
    //
    protected $table = 'cliente';
    protected $primaryKey = 'idcliente';
    public static $key = 'idcliente';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'idcliente',
    'nombre',
    'apellidos',
    'telefono',
    'email',
    'password',
    'colonia',
    'calle',
    'numero',
    'fecharegistro' ,
    'fechamodificacion'];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function ordenes()
    {
        return $this->hasMany('App\Orden');
    }
}
