<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Envio extends Model
{
    //
	protected $table = 'envio';
    protected $primaryKey = 'idenvio';
    public static $key = 'idenvio';
    public $timestamps = false;
    protected $fillable = [
    'nombre',
    'direccion',
    'venta_idventa',
    'telefono'];
}
