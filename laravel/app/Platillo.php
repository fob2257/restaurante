<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platillo extends Model
{
    //
	protected $table = 'platillo';
    protected $fillable = [
    'idplatillo',
    'nombre',
    'precio'];
    public function pedidos()
    {
        return $this->hasMany('App\Pedido');
    }

    public function ingredientes()
    {
        return $this->belongsToMany('App\Ingrediente');
    }
}
