<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    //protected $table = 'ticket';

	protected $table = 'ticket';
    protected $primaryKey = 'no_folio';
    public static $key = 'no_folio';
    public $timestamps = false;
    protected $fillable = [
    'no_folio',
    'transaccion_idtransaccion'];
}
