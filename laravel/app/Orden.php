<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use View;
use DB;

class Orden extends Model
{
    //
	protected $table = 'orden';
    protected $primaryKey = 'idorden';
    public static $key = 'idorden';
    public $timestamps = false;
    protected $fillable = [
    'idorden',
    'importe',
    'estado',
    'fecharegistro'];

	public function ticket()
	{
		return $this->hasOne('App\Ticket');
	}
	public function cliente()
	{
		return $this->belongsTo('App\Cliente', 'cliente_idcliente');
	}
	  public function pedidos()
    {
        return $this->hasMany('App\Producto');
    }
}
