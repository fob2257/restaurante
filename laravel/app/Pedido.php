<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
 

    //
    protected $table = 'pedido';
    public $timestamps = false;
    protected $fillable = [
    'idpedido',
    'cantidad',
    'precio',
    'orden_idorden',
    'orden_cliente_idcliente',
    'extra_idextra',
    'platillo_idplatillo'];

	public function orden()
	{	
		return $this->belongsTo('App\Orden', 'orden_idorden	');
	}
		public function cliente()
	{
		return $this->belongsTo('App\Cliente', 'orden_cliente_idcliente	');
	}
		public function platillo()
	{
		return $this->hasOne('App\Platillo', 'platillo_idplatillo');
	}
}
