<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use View;
use DB;

class Venta extends Model
{
    //
	protected $table = 'venta';
    protected $primaryKey = 'idventa';
    public static $key = 'idventa';
    public $timestamps = false;
    protected $fillable = [
    'idventa',
    'importe',
    'estado',
    'fecharegistro'];

	public function ticket()
	{
		return $this->hasOne('App\Ticket');
	}
	public function cliente()
	{
		return $this->belongsTo('App\Cliente', 'cliente_idcliente');
	}
	  public function pedidos()
    {
        return $this->hasMany('App\Pedido');
    }
}
