@extends('layout')
@section('title', 'Información')
@section('content')


<div class="container" style=" padding-top: 100px;">
  <div class="jumbotron">
    <h1 style="text-align:center;">Información bancaria</h1>
  </div>
</div>



<div class="container">
  <div class="jumbotron" style="height:300px">

    &nbsp;
    
    <!-- {!!Form::open(['route'=>'envio.store','method'=>'POST','class'=>'form-horizontal'])!!} -->


      <div class="form-group">
        <label class="col-sm-2 control-label">Número de tarjeta:</label>
        <div class="col-sm-10">
          {!!Form::text('numtarjeta',null,['class'=>'form-control','required','autofocus'])!!}
       </div>
     </div>

     <div class="form-group">
      <label class="col-sm-2 control-label">Vigencia:</label>
      <div class="col-sm-10">
        <!-- {!!Form::text('vigencia',null,['class'=>'form-control','onkeypress'=>'return validar(event)','required','autofocus'])!!} -->
        <input type="number" name="month" value="1" min="1" max="12">/
        <input type="number" name="year" value="16" min="16" max="22">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Dígitos de verificación:</label>
      <div class="col-sm-10">
        {!!Form::text('verificacion',null,['class'=>'form-control','required','autofocus'])!!}
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-3">
        <a href="/domicilio">
          <button type="button" class="btn btn-warning btn-lg btn-block">Regresar a domicilio</button>
        </a>
      </div>

      <div class="col-md-3">
        <button type="button" name="button" class="btn btn-warning btn-lg btn-block" onclick="solicitarTransferencia()">Pagar</button>
      </div>
    </div>
    
  </div>
  <meta name="csrf-token" content="{!! Session::token() !!}">
</div>
{!! Html::script('js/banco.js') !!}
@endsection
