@extends('layout')
@section('title', 'Menu')
@section('content')

<style>
  .item #boton{
    padding-top: 200%;
  }
  .item .content img{
    height: 220px;
    width: 390px;
  }
  .content h3{
    text-align:center;
  }
</style>
<div class="container" style=" padding-top: 50px;">
  <div class="jumbotron">
    <h1 style="text-align:center;">Facturas</h1>
  </div>
</div>

<div class="container">
  <div class="jumbotron">
    <div class="table-responsive">
      <table class="table">
        <h4>Folio de Factura: {{$folio}}</h4>
        <h4>Fecha de expedición: {{$fecha}}</h4>
        <h4>Importe: {{$importe}}</h4>

        <thead>
          <tr>
            <th>Datos del receptor</th>
            <th>Datos del emisor</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Nombre: {{ Auth::user()->nombre }}</td>
            <td>Nombre: Le Gourmet</td>
          </tr>
          <tr>
            <td>Domicilio: {{$domicilio}}</td>
            <td>Domicilio: Plaza Sendero</td>
          </tr>
        </tbody>
      </table>

      <table class="table">
      <thead>
        <tr>
          <th>Producto</th>
          <th>Cantidad</th>
          <th>Total: cantidad + IVA</th>
        </tr>
      </thead>
      @foreach($producto as $pro => $value)


      <tbody>
        <tr>
          <td>{{ $value->descripcion }}</td>
          <td>{{ $value->cantidad }}</td>
          <td>${{ $value->importe }}</td>
        </tr>
      </tbody>
      @endforeach
      </table>
    </div>
  </div>
</div>

@endsection
