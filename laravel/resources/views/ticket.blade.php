@extends('layout')
@section('title', 'Compras')
@section('content')

<div class="container" style="padding-top: 100px;">
  <div class="jumbotron">
    <h1>Mis compras</h1>
  </div>
      @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}

    </div>
    @endif
	<!-- Menú -->
	<div class="jumbotron">
		@if(isset($datosTicket))
		<div class="table-responsive">

			@for ($i = 0; $i < count($datosTicket); $i++)
			<table class="table">
      <h3>
      	Empresa: Le Gourmet<br>
        Folio de ticket: {{ $datosTicket[$i][6] }}<br>
        Importe: ${{ $datosTicket[$i][0] }}<br>
        Fecha de creación: {{ $datosTicket[$i][1] }}<br>
        Estado de orden: 
				@if ($datosTicket[$i][2] == 2)
					Pago realizado
				@elseif ($datosTicket[$i][2] == 1)
					Pago fallido
				@else
					Sin pagar
				@endif
				<br>
        Número de transacción: {{ $datosTicket[$i][4] }}<br>
      </h3>

  		<thead>
  			<tr>
  				<th>Producto</th>
  				<th>Cantidad</th>
  				<th>Total: cantidad + IVA</th>
  			</tr>
  		</thead>

  		<tbody>
			  @for ($j = 0; $j < count($datosPro[$i]); $j++)
					<tr>
					  <td>{{ $datosPro[$i][$j][2] }}</td>
					  <td>{{ $datosPro[$i][$j][1] }}</td>
					  <td>${{ $datosPro[$i][$j][0] }}</td>
				  </tr>
				@endfor
      <a href="/factura-{{$datosTicket[$i][7]}}">
        <button type="button" name="{{ $datosTicket[$i][7] }}" class="btn btn-success">
          Facturar compra <span class="glyphicon glyphicon-credit-card"></span>
        </button>
      </a>
			</tbody>
			@endfor
		  </table>
		</div>
		@endif
	</div>
	<meta name="csrf-token" content="{!! Session::token() !!}">
</div>

@endsection
