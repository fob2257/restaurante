@extends('layout')
@section('title', 'Menu')
@section('content')

<style>
  .item #boton{
    padding-top: 220%;
  }
  .item .content img{
    height: 220px;
    width: 390px;
  }
  .content h3{
    text-align:center;
  }
</style>

<div class="container">
  <div class="jumbotron">
    <h1 style="text-align:center;">Menú</h1>
  </div>
</div>

<div class="container">
  <div class="jumbotron">
    @if(Auth::check())

    <div class="form-group">
      <label class="col-sm-2 control-label">Nombre:</label>
      <div class="col-sm-10">
        {!!Form::text('nombre',null,['class'=>'form-control','placeholder'=>'e.g. Alan','onkeypress'=>'return validar(event)','required','autofocus'])!!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Dirección:</label>
      <div class="col-sm-10">
        {!!Form::text('direccion',null,['class'=>'form-control','placeholder'=>'e.g. Calle 100 col. el ranchito','onkeypress'=>'return validar(event)','required'])!!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Número telefónico:</label>
      <div class="col-sm-10">
        {!!Form::tel('telefono',null,['class'=>'form-control','placeholder'=>'e.g. 6621567890','onkeypress'=>'return validar(event)','required', 'pattern'=>'[0-9.]*'])!!}
      </div>
    </div>

    <input type="image" name="name" src="app\imgs\carrito.png" style="margin-left:45%;" width="48" height="48" data-toggle="modal" data-target="#myModal">
    @else
    <a href="/auth/login">
      <button type="button" class="btn btn-default btn-lg">Inicie sesión para ordenar</button>
    </a>
    @endif
    <!-- Menú -->
    <div class="table-responsive">
      <table class="table">
        <tbody>
          <td>
            @foreach($platillo as $plat => $value)
            <div class="col-md-3">
              <div class="item">
                <div class="content">
                  <a><img src="{{ $value->imagen }}" class="img-circle img-responsive"/></a>
                  <h3>
                    <span class="label label-default">
                      {{ $value->nombre }} - ${{ $value->precio }}
                    </span>
                  </h3>
                  @if(Auth::check())
                  <input type="number" name="{{ $value->idplatillo }}" value="1" min="1" max="9">
                  <input class="btn btn-default" type="button" value="Añadir al carrito" id="{{ $value->idplatillo }}" onclick="introducePlatillo(id, '{{ $value->nombre }}', {{ $value->precio }})">
                  @else

                  @endif
                </div>
              </div>
            </div>
            @endforeach
          </td>
        </tbody>
      </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="container">
            <div class="row">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th class="text-center">Producto</th>
                      <th class="text-center">Precio</th>
                      <th>Precio + IVA</th>
                      <th>Cantidad</th>
                      <th>Total</th>
                      <th>Quitar</th>
                      <th> </th>
                    </tr>
                  </thead>

                  <tbody  name="carrito">

                  </tbody>

                  <tbody  name="total">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h3>Total</h3></td>
                        <td></td>
                        <td class="text-right"><h3><strong></strong></h3></td>
                    </tr>
                    <tr>
                      <td>   </td>
                      <td>   </td>
                      <td>   </td>
                      <td>
                      <button type="button" class="btn btn-default" data-dismiss="modal">
                        <span class="glyphicon glyphicon-shopping-cart"></span> Continuar comprando
                      </button></td>
                      <td>
                        <a href="/domicilio">
                          <button type="button" class="btn btn-success" onclick="enviarOrden()">
                            Finalizar <span class="glyphicon glyphicon-credit-card"></span>
                          </button>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <meta name="csrf-token" content="{!! Session::token() !!}">
</div>

{!! Html::script('js/carrito.js') !!}

@endsection
