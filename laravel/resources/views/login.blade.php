@extends('layout')
@section('title', 'Clientes')
@section('content')


<div class="container">
  <div class="jumbotron">
    <h1 style="text-align:center;">Iniciar sesión</h1>
  </div>
</div>
<div class="container">
  <div class="jumbotron">

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Whoops!</strong> Hay algunos problemas con tus datos.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
        <li>{!! $error !!}</li>
        @endforeach
      </ul>
    </div>
    @endif

    @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}

    </div>
    @endif
    &nbsp;

       {!!Form::open(['route'=>'auth/login','method'=>'POST','class'=>'form-horizontal'])!!}
      {!! csrf_field() !!}
    
     <div class="form-group  form-group-lg">
      <label class="col-sm-2 control-label">Email:</label>
      <div class="col-sm-10">
        {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'e.g. scott@gmail.com','required'])!!}
      </div>
    </div>
     <div class="form-group form-group-lg">
      <label class="col-sm-2 control-label">Contraseña:</label>
      <div class="col-sm-10">
        {!!Form::password('password',['class'=>'form-control','placeholder'=>'Contraseña','required'])!!}
      </div>
    </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <a href="/registrarse">
            <p style="text-align: center;">Aún no estas registrado ?</p>
          </a>
        </div>
      </div>

      <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        {!!Form::submit('Iniciar sesión',['class'=>'btn btn-success btn-lg btn-block'])!!}
      </div>
    </div>
    
    {!!Form::close()!!}
  </div>
</div>



@endsection
