@extends('layout')
@section('title', 'Inicio')
@section('content')
<style>
  h1, h2, h3, p{
    text-align:center;
  }
  .item .content a{
    /*display: block;*/
    width: 250px;
  }
  .item .content a img {
    height: 150px;
    width: 225px;
  }
  #fondocarousel{
    padding:10px;
    background-color:black;
    opacity: 0.7;
    filter: alpha(opacity=70);
  }
  #fondocarousel:hover{
    opacity: 1.0;
    filter: alpha(opacity=100); /* For IE8 and earlier*/
  }
  #imgcarousel{
    margin-right: auto;
    margin-left: auto;
    margin-top: -420px;
  }
</style>

<div class="jumbotron" style="padding-bottom: 10px;">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img id="imgcarousel" src="app\imgs\back.jpg" alt="Le Gourmet"/>
      </div>
      <div class="item">
        <img id="imgcarousel" src="app\imgs\back2.jpg" alt="" />
      </div>
      <div class="item">
        <img id="imgcarousel" src="app\imgs\back3.jpeg" alt="" />
      </div>
        <div class="carousel-caption">
          <div id="fondocarousel">
            <h1>Le Gourmet</h1>
            <hr>
            <p>Restaurante Francés</p>
            &nbsp;
          </div>
        </div>
    </div>
  </div>
</div>

<div class="container">
  <div id="iniciar" class="row row-centered">
    <div class="col-md-4 col-centered">
      <div class="item">
        <div class="content">
          @if( Auth::check() )

          @else
            <a href="/auth/login" >
              <img src="app\imgs\client.jpg" alt="Clientes" class="img-circle img-responsive center-block">
            </a>
            <h3><kbd>Iniciar Sesión</kbd></h3>
          @endif
        </div>
      </div>
    </div>

    <div class="col-md-4 col-centered">
      <div class="item">
        <div class="content">
          <a href="/armar">
            <img src="app\imgs\menu.jpg" alt="Menúclientes" class="img-circle img-responsive center-block">
          </a>
          <h3><kbd>Menú</kbd></h3>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-centered">
      <div class="item">
        <div class="content">
          @if(Auth::check())
          <a href="/ticket" >
            <img src="app\imgs\contact.jpg" alt="Tickets" class="img-circle img-responsive center-block">
          </a>
          <h3><kbd>Tickets</kbd></h3>
          @else
            <a href="/registrarse" >
              <img src="app\imgs\newclient.jpg" alt="Nuevos clientes" class="img-circle img-responsive center-block">
            </a>
            <h3><kbd>Registrarse</kbd></h3>
          @endif
        </div>
      </div>
    </div>
    
  </div>
</div>

@endsection
