<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="icon" href="app\imgs\favicon.ico">
  <title>Le Gourmet- @yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{ asset('css/layout.css') }}" rel="stylesheet">

</head>
<body id="bgcolor" onload="document.getElementById('bgcolor').style.visibility = 'visible';" style=" visibility: hidden;">  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand">Le Gourmet
          @if( Auth::check() )
              - Bonjour!
              {{ Auth::user()->nombre }}
          @endif
        </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        @if( Auth::check() )
        <ul class="nav navbar-nav">
          <li><a href="/">Inicio</a></li>
          <li><a href="/armar">Menú</a></li>
          <li><a href="/ticket">Tickets</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="/auth/logout">Cerrar sesión</a></li>
        </ul>
        @else
        <ul class="nav navbar-nav">
          <li><a href="/">Inicio</a></li>
          <li><a href="/armar">Menú</a></li>
          <li><a href="/auth/login">Iniciar Sesión</a></li>
          <li><a href="/registrarse">Registrarse</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="/contacto">Contacto</a></li>
        </ul>
        @endif
      </div>
    </div>
  </nav>
  @yield('content')
</body>
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}">
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</html>
