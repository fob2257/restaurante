@extends('layout')
@section('title', 'Registrarse')
@section('content')


<div class="container" style=" padding-top: 100px;">
  <div class="jumbotron">
    <h1 style="text-align:center;">Registro</h1>
  </div>
</div>



<div class="container">
  <div class="jumbotron">
  @if (count($errors) > 0)
<div class="alert alert-danger">
  <strong>Whoops!</strong> Hay algunos problemas con tus datos.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

    &nbsp;

      {!!Form::open(['route'=>'cliente.store','method'=>'POST','class'=>'form-horizontal'])!!}
      {!! csrf_field() !!}

      <div class="form-group">
        <label class="col-sm-2 control-label">Nombre:</label>
        <div class="col-sm-10">
         {!!Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Nombre','onkeypress'=>'return validar(event)','required','autofocus'])!!}
       </div>
     </div>

     <div class="form-group">
      <label class="col-sm-2 control-label">Apellidos:</label>
      <div class="col-sm-10">
        {!!Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'Apellidos','onkeypress'=>'return validar(event)','required'])!!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Teléfono:</label>
      <div class="col-sm-10">
        {!!Form::tel('telefono',null,['class'=>'form-control','placeholder'=>'e.g. 6621573739','onkeypress'=>'return validar(event)','required', 'pattern'=>'[0-9.]*'])!!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Email:</label>
      <div class="col-sm-10">
        {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'e.g. scott@gmail.com','required'])!!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Contraseña:</label>
      <div class="col-sm-10">
        {!!Form::password('password',['class'=>'form-control','placeholder'=>'Contraseña','required'])!!}
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Verificar contraseña:</label>
      <div class="col-sm-10">
        {!!Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirmar contraseña','required'])!!}
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        {!!Form::submit('Registro',['class'=>'btn btn-success btn-lg btn-block'])!!}
      </div>
    </div>


  {!!Form::close()!!}

  </div>
</div>






@endsection
