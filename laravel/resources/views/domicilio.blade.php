@extends('layout')
@section('title', 'Información')
@section('content')

<div class="container" style=" padding-top: 100px;">
  <div class="jumbotron">
    <h1 style="text-align:center;">Ingrese información bancaria</h1>
  </div>
</div>

<div class="container">
  <div class="jumbotron">
    &nbsp;
    {!!Form::open(['route'=>'envio.store','method'=>'POST','class'=>'form-horizontal'])!!}
    {!! csrf_field() !!}

    <div class="form-group">
      <label class="col-sm-2 control-label">Número de tarjeta:</label>
        <div class="col-sm-10">
          {!!Form::text('numtarjeta',null,['class'=>'form-control','required','autofocus'])!!}
        </div>
    </div>
    
    <div class="form-group">
      <label class="col-sm-2 control-label">Vigencia:</label>
      <div class="col-sm-10">
        <input type="number" name="month" value="1" min="1" max="12">
        <input type="number" name="year" value="16" min="16" max="22">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label">Dígitos de verificación:</label>
      <div class="col-sm-10">
        {!!Form::text('verificacion',null,['class'=>'form-control','required','autofocus'])!!}
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        {!!Form::submit('Pagar',['class'=>'btn btn-success btn-lg btn-block'])!!}
      </div>
    </div>
    
    {!! Form::hidden('id', $id) !!}
    {!! Form::hidden('importe', $importe) !!}
    {!! Form::hidden('venta', $venta) !!}
    {!!Form::close()!!}
  </div>
</div>

@endsection
