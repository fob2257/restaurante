angular.module('myapp',['ngRoute'])

.config(function($routeProvider) {
  $routeProvider.when("/",
    {
      templateUrl:"app/views/home.html"
    }
  )

  .when("/menu",
    {
      templateUrl:"app/views/menu.html"
    }
  )

  .when("/clientes",
    {
      templateUrl:"app/views/clientes.html"
    }
  )

  .when("/registrarse",
    {
      templateUrl:"app/views/registrarse.html"
    }
  )

  .when("/contacto",
    {
      templateUrl:"app/views/contacto.html"
    }
  )

  .otherwise("/404",
    {
      templateUrl:"app/views/404.html"
    }
  );
});
